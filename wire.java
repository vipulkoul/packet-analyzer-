/**
 * wire.java
 *
 * Version : 1.0
 *
 * Revisions : 0
 */


/**
 * This program processes the ICMP,TCP and UDP packets in IPv4 format to give
 * us the necessary information regarding the packet. The packet data is in 
 * encoded HexaDecimal format which has to be decoded into a normal HexString
 * format for the data to be processed. At some stages the HexaDecimal values 
 * are converted to either Decimal format or Binary format as needed.
 * @Author    Vipul Koul
 */


import java.util.*;
import java.io.*;
import java.nio.file.Files;
import javax.xml.bind.DatatypeConverter;
import java.net.*;



class wire{

	/**
	 * This function bth takes in the encoded HexaDecimal 
	 * byte array and converts it into a String which has 
	 * the encoded HexDecimal values.	
	 */

	public static String bth(byte[] pack){
		final StringBuilder builder = new StringBuilder();
		for(byte p : pack)
			builder.append(Integer.toString(p,16));
		return builder.toString();
	}

	/**
	 * This function convertHexToDec takes in a String and converts it into a 
	 * integer in decimal values.
	 */



	public static int convertHexToDec(String st){
		return Integer.parseInt(st,16);
	}

	/**
	 * This is the main function which initates the file read of .pcap
	 * format and then it calls the above function for execution.
	 * The packet is then formatted accordingly to the format and
	 * in the order they appear.	
	 */



	public static void main(String args[])throws IOException {
		File file = new File(args[0]);// Reading file from the command line
		byte[] pack = Files.readAllBytes(file.toPath());// Storing the file contents as byte array
		String encoded_hexpack = bth(pack);// This is the encoded the HexString value
		String hexstring = DatatypeConverter.printHexBinary(pack);// This is the decoded hexstring value 
		hexstring = hexstring.substring(80,hexstring.length());// This stores the length of the packet
		int packet_size=(hexstring.length())/2;// The length of packet is divided by 2 to get the bytes value
		int packet_type=convertHexToDec(hexstring.substring(46,48));// This is to get the version of the packet type
 		System.out.println("      ---ETHER HEADER---");// This is the start of the Ether Header section
		System.out.println("ETHER: ");
		System.out.println("ETHER: Packet Size = "+packet_size+" bytes");
		System.out.print("ETHER: Destination = "); // For displaying the destination mac address 
		for(int i=0;i<10;i=i+2){
			System.out.print(hexstring.substring(0+i,2+i)+":");
		}
		System.out.println(hexstring.substring(10,12)+",");
		System.out.print("ETHER: Source = "); // For displaying the source mac address 
		for(int i=0;i<10;i=i+2){
			System.out.print(hexstring.substring(12+i,14+i)+":");
		}
		System.out.println(hexstring.substring(22,24)+",");
		String ether_type=hexstring.substring(24,28);
		if(ether_type.equals("0800")){
			ether_type="IPv4";
			System.out.println("ETHER: EtherType = "+hexstring.substring(24,28)+" ("+ether_type+")");// Displaying Ether type
			System.out.println("ETHER: ");
			System.out.println("      ---IP HEADER---");
			System.out.println("IP: ");
			System.out.println("IP: Version = "+ether_type);// Displaying IP version
			System.out.println("IP: Header length ="+ (hexstring.substring(33,73).length())/2+" bytes");// Displaying Header Length
			System.out.println("IP: Type of Service = 0x"+hexstring.substring(31,33));// Displaying Type of service
			int ip_type=convertHexToDec(hexstring.substring(31,33));
			String ip_type_temp=Integer.toBinaryString(ip_type);
			if(ip_type_temp.equals("0")){ // Displaying the format for type of service
			
				System.out.println("IP:       xxx. .... = 0 (precedence)");
				System.out.println("IP:       ...0 .... = normal delay");
				System.out.println("IP:       .... 0... = normal throughput");
				System.out.println("IP:       .... .0.. = normal reliability");
			}
			System.out.println("IP: Total length = "+convertHexToDec(hexstring.substring(32,36)));// Displaying total length
			System.out.println("IP: Identification = "+convertHexToDec(hexstring.substring(36,40)));// Displaying Identification 
			System.out.println("IP: Flags = 0x"+hexstring.substring(40,44));// Displaying the flag values
			int ip_flag=convertHexToDec(hexstring.substring(40,44));
			String ip_flag_temp=Integer.toBinaryString(ip_flag);
			String ip_binary=new String("0");
			ip_binary=ip_binary+ip_flag_temp; 
			if(ip_binary.substring(0,1).equals("0"))// Checking the flag bits for Reserved and Don't Fragment value
				System.out.println(" \tReserved bit: Not set");
			else
				System.out.println(" \tReserved bit: Set");
			if(ip_binary.substring(1,2).equals("0"))
				System.out.println(" \tDon't Fragment: Not set");
			else
				System.out.println(" \tDon't Fragment: Set");

			System.out.println("IP: Fragment offset = "+convertHexToDec(hexstring.substring(43,44))+" bytes");// Displaying Fragment Offset
			System.out.println("IP: Time to Live = "+convertHexToDec(hexstring.substring(44,46))+" hops/secs");// Displaying Time to Live
			System.out.print("IP: Protocol = "+packet_type);// Displaying IP Protocol
			if(packet_type==1)
				System.out.print(" (ICMP)\n");
			else if(packet_type==6)
				System.out.print(" (TCP)\n");
			else
				System.out.print(" (UDP)\n");
			System.out.println("IP: Header Checksum = "+hexstring.substring(48,52));// Displaying the Header Checksum
			System.out.print("IP: Source Address = ");
			String source_add=new String("");
			for(int i=0;i<6;i=i+2){
				source_add=source_add+convertHexToDec(hexstring.substring(52+i,54+i))+".";
			}
			source_add=source_add+convertHexToDec(hexstring.substring(58,60));
			System.out.print(source_add+"\t");// Display the source address
			InetAddress add=InetAddress.getByName(source_add);
			String source_add_hn=add.getHostName();// Gets the host name for the address
			System.out.print(source_add_hn+"\n");
			System.out.print("IP: Destination Address = ");
			String dest_add=new String("");
			for(int i=0;i<6;i=i+2){
				dest_add=dest_add+convertHexToDec(hexstring.substring(60+i,62+i))+".";
			}
			dest_add=dest_add+convertHexToDec(hexstring.substring(66,68));
			System.out.print(dest_add+"\t");// Display the dest address
			InetAddress add1=InetAddress.getByName(dest_add);
			String dest_add_hn=add1.getHostName();// Gets the host name for the address
			System.out.print(dest_add_hn+"\n");
			System.out.println("IP: No Options");

			
		}
		else
			ether_type="IPv6";

		
		if(packet_type==1 && ether_type.equals("IPv4")){
			System.out.println("      ---ICMP HEADER---");//Start of the IP Header
			System.out.println("ICMP:");
			System.out.println("ICMP: Type = "+convertHexToDec(hexstring.substring(68,70)));
			System.out.println("ICMP: Code = "+convertHexToDec(hexstring.substring(70,72)));
			System.out.println("ICMP: Checksum = "+(hexstring.substring(72,76)));
			System.out.println("ICMP: ");

		}
		else if(packet_type==6 && ether_type.equals("IPv4")){
			System.out.println("      ---TCP HEADER---");
			System.out.println("TCP: Source Port = "+convertHexToDec(hexstring.substring(68,72)));
			System.out.println("TCP: Destination Port = "+convertHexToDec(hexstring.substring(72,76)));
			System.out.println("TCP: Sequence Number = "+convertHexToDec(hexstring.substring(76,82)));
			System.out.println("TCP: Ack Number = "+convertHexToDec(hexstring.substring(82,88)));
			System.out.println("TCP: Data Offset = "+(convertHexToDec(hexstring.substring(92,93)))*4+" bytes");
			System.out.println("TCP: Flags = 0x"+hexstring.substring(93,96));//Displaying the TCP Flags
			int tcp_flag=convertHexToDec(hexstring.substring(93,96));
			String tcp_flag_temp=Integer.toBinaryString(tcp_flag);
			String tcp_binary=new String("");
			for(int i=0;i<(9-tcp_flag_temp.length());i++)
				tcp_binary=tcp_binary+"0";
			tcp_binary=tcp_binary+tcp_flag_temp;
			if(tcp_binary.substring(0,1).equals("1"))//Displaying the TCP Flag values
				System.out.println("TCP: 		Nonce set");
			else
				System.out.println("TCP: 		Nonce not set");
			if(tcp_binary.substring(1,2).equals("1"))
				System.out.println("TCP: 		CWR set");
			else
				System.out.println("TCP: 		CWR not set");
			if(tcp_binary.substring(2,3).equals("1"))
				System.out.println("TCP: 		ECN-Echo set");
			else
				System.out.println("TCP: 		ECN-Echo not set");
			if(tcp_binary.substring(3,4).equals("1"))
				System.out.println("TCP: 		Urgent set");
			else
				System.out.println("TCP: 		Urgent not set");
			if(tcp_binary.substring(4,5).equals("1"))
				System.out.println("TCP: 		Ack set");
			else
				System.out.println("TCP: 		Ack not set");
			if(tcp_binary.substring(5,6).equals("1"))
				System.out.println("TCP: 		Push set");
			else
				System.out.println("TCP: 		Push not set");
			if(tcp_binary.substring(6,7).equals("1"))
				System.out.println("TCP: 		Reset set");
			else
				System.out.println("TCP: 		Reset not set");
			if(tcp_binary.substring(7,8).equals("1"))
				System.out.println("TCP: 		Syn set");
			else
				System.out.println("TCP: 		Syn not set");
			if(tcp_binary.substring(8,9).equals("1"))
				System.out.println("TCP: 		Fin set");
			else
				System.out.println("TCP: 		Fin not set");
			System.out.println("TCP: Window = "+convertHexToDec(hexstring.substring(96,100)));
			System.out.println("TCP: Checksum = 0x"+hexstring.substring(100,104));
			System.out.println("TCP: Urgent Pointer = "+convertHexToDec(hexstring.substring(104,108)));
			System.out.println("TCP: Data ");
			int data_length=hexstring.length()-108;
			if(data_length==0)
				System.out.println("TCP: No data here ");
			else{
				try{
					for(int i=132;(i<hexstring.length() )&&(i<196);i=i+7){
						System.out.print("\nTCP: \t");
						for(int j=0;j<28;j=j+4)
							System.out.print(hexstring.substring(i+j,i+j+4)+"\t");
					}
				}
				catch(IndexOutOfBoundsException e){
					System.out.println("End of packet");
				}
			}
			System.out.println("End of packet");

		}
		else{
			System.out.println("      ---UDP HEADER---");//Displaying the UDP header
			System.out.println("UDP: ");
			System.out.println("UDP: Source Port = "+convertHexToDec(hexstring.substring(68,72)));
			System.out.println("UDP: Destination Port = "+convertHexToDec(hexstring.substring(72,76)));
			System.out.println("UDP: Source Port = "+convertHexToDec(hexstring.substring(76,80)));
			System.out.println("UDP: Checksum = 0x"+hexstring.substring(80,84));
			System.out.println("UDP: Data ");
			int data_length=hexstring.length()-84;
			if(data_length==0)
				System.out.println("UDP: No data here ");
			else{
				try{
					for(int i=84;(i<hexstring.length() )&&(i<148);i=i+7){
						System.out.print("\nTCP: \t");
						for(int j=0;j<28;j=j+4)
							System.out.print(hexstring.substring(i+j,i+j+4)+"\t");
					}
				}
				catch(IndexOutOfBoundsException e){
					System.out.println("End of packet");
				}
			}
			System.out.println("End of packet");

		}

		

	}
}